/* This file is managed by @html-validate/eslint-config */
/* Changes may be overwritten */

require("@html-validate/eslint-config/patch/modern-module-resolution");

module.exports = {
	root: true,
	extends: ["@html-validate"],

	overrides: [
		{
			/* ensure cjs and mjs files are linted too */
			files: ["*.cjs", "*.mjs"],
		},
		{
			files: "*.ts",
			extends: ["@html-validate/typescript"],
		},
		{
			files: ["src/**/*.ts"],
			excludedFiles: ["src/**/*.spec.ts"],
			parserOptions: {
				tsconfigRootDir: __dirname,
				project: ["./tsconfig.json"],
			},
			extends: ["@html-validate/typescript-typeinfo"],
		},
		{
			files: "*.spec.[jt]s",
			excludedFiles: ["cypress/**", "tests/e2e/**"],
			extends: ["@html-validate/jest"],
		},
		{
			files: ["cypress/**/*.spec.[jt]s", "cypress/**/*.cy.[jt]s"],
			extends: ["@html-validate/cypress"],
		},
		{
			/* files which should lint even if project isn't build yet */
			files: ["./*.d.ts", "bin/*.js"],
			rules: {
				"import/export": "off",
				"import/extensions": "off",
				"import/no-unresolved": "off",
			},
		},
	],
};
