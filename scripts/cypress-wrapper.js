const { spawn } = require("child_process");
const semver = require("semver");
const pkg = require("../package.json");

/** @type {Map<string, string>} */
const configFiles = new Map([
	[7, `cypress/v7/cypress.json`],
	[8, `cypress/v7/cypress.json`],
	[9, `cypress/v7/cypress.json`],
	[10, `cypress/v10/cypress.config.ts`],
	[11, `cypress/v10/cypress.config.ts`],
	[12, `cypress/v10/cypress.config.ts`],
	[13, `cypress/v10/cypress.config.ts`],
	[14, `cypress/v10/cypress.config.ts`],
]);

const version = process.env.CYPRESS_MATRIX
	? parseInt(process.env.CYPRESS_MATRIX, 10)
	: semver.major(pkg.devDependencies.cypress);

if (!configFiles.has(version)) {
	throw new Error(`Failed to find configuration file for Cypress v${version}`);
}

const configFile = configFiles.get(version);

/* eslint-disable-next-line no-console -- expected to log */
console.log(`Using Cypress v${version} (${configFile})`);

const command = process.argv[2];
const builtinArgs = ["--config-file", configFile];
const userArgs = process.argv.slice(3);

spawn("./node_modules/.bin/cypress", [command, ...builtinArgs, ...userArgs], {
	stdio: "inherit",
}).on("exit", (code) => {
	process.exitCode = code;
});
