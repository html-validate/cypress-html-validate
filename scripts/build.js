const path = require("path");
const esbuild = require("esbuild");

/** @type {import("esbuild").BuildOptions} */
const common = {
	logLevel: "info",
	bundle: true,
	platform: "node",
	target: "node18",
	supported: {
		"nullish-coalescing": false,
	},
	sourcemap: true,
	external: ["html-validate", path.join(__dirname, "../package.json")],
};

esbuild.buildSync({
	...common,
	entryPoints: ["src/plugin.ts"],
	outfile: "dist/plugin.js",
});

esbuild.buildSync({
	...common,
	entryPoints: ["src/commands.ts"],
	outfile: "dist/commands.js",
});

esbuild.buildSync({
	...common,
	entryPoints: ["src/index.ts"],
	outfile: "dist/index.js",
});
