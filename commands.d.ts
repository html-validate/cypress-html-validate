/// <reference types="cypress" />

import type { ConfigData } from "html-validate";
import type { CypressHtmlValidateOptions } from "cypress-html-validate";

declare global {
	export namespace Cypress {
		interface Chainable {
			htmlvalidate(
				localConfig?: ConfigData,
				localOption?: Partial<CypressHtmlValidateOptions>,
			): void;
			htmlvalidate(localOption: Partial<CypressHtmlValidateOptions>): void;
		}
	}
}

export {};
