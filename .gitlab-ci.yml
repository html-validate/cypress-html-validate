image: node:22

stages:
  - prepare
  - build
  - test
  - compatibility
  - release

NPM:
  stage: prepare
  artifacts:
    name: ${CI_PROJECT_PATH_SLUG}-${CI_PIPELINE_ID}-npm
    paths:
      - node_modules/
  variables:
    CYPRESS_INSTALL_BINARY: "0"
  script:
    - node --version
    - npm --version
    - npm ci --no-fund --no-audit --no-update-notifier
    - npm audit --json --production | npx --yes gitlab-npm-audit-parser -o gl-dependency-scanning.json || true
    - test -z "${UPSTREAM_VERSION}" || npm install "html-validate@${UPSTREAM_VERSION}"

Build:
  stage: build
  artifacts:
    name: ${CI_PROJECT_PATH_SLUG}-${CI_PIPELINE_ID}-dist
    paths:
      - dist/
  script:
    - npm run --if-present build
    - npm pack
    - npm exec npm-pkg-lint

ESLint:
  stage: test
  script:
    - npm exec eslint-config -- --check
    - npm run eslint -- --max-warnings 0

Prettier:
  stage: test
  script:
    - npm run prettier:check

Cypress:
  stage: test
  image: cypress/browsers:node-18.16.1-chrome-114.0.5735.133-1-ff-114.0.2-edge-114.0.1823.51-1
  before_script:
    - node --version
    - google-chrome --version
    - npm rebuild cypress
  script:
    - npm test

.compat:
  stage: compatibility
  image: cypress/browsers:node-18.16.1-chrome-114.0.5735.133-1-ff-114.0.2-edge-114.0.1823.51-1
  needs: ["NPM", "Build"]
  script:
    - npm rebuild cypress
    - npm test -- --no-coverage tests

HTML-validate:
  extends: .compat
  parallel:
    matrix:
      - VERSION:
          - 8
          - 9
  before_script:
    - npm install $(npx -y npm-min-peer html-validate --major ${VERSION} --with-name)
    - npm ls html-validate

Cypress compatibility:
  extends: .compat
  parallel:
    matrix:
      - CYPRESS_MATRIX:
          - 11
          - 12
          - 13
          - 14
  before_script:
    - npm install $(npx -y npm-min-peer cypress --major ${CYPRESS_MATRIX} --with-name)
    - npm ls cypress

.release:
  stage: release
  variables:
    GIT_AUTHOR_NAME: ${GITLAB_USER_NAME}
    GIT_AUTHOR_EMAIL: ${GITLAB_USER_EMAIL}
    GIT_COMMITTER_NAME: ${HTML_VALIDATE_BOT_NAME}
    GIT_COMMITTER_EMAIL: ${HTML_VALIDATE_BOT_EMAIL}
  before_script:
    - npm install -g $(node -p 'require("./package.json").release.extends')

Dry run:
  extends: .release
  needs: []
  dependencies: []
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
    - if: '$CI_COMMIT_REF_NAME =~ /^release\//'
  script:
    - npm exec semantic-release -- --dry-run

Release:
  extends: .release
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master" && $CI_PIPELINE_SOURCE == "web"'
      when: manual
    - if: '$CI_COMMIT_REF_NAME =~ /^release\// && $CI_PIPELINE_SOURCE == "web"'
      when: manual
  script:
    - npm exec semantic-release
