const express = require("express");
const serveStatic = require("serve-static");

/* eslint-disable-next-line sonarjs/x-powered-by -- used for tests only */
const app = express();
app.use(serveStatic("pages"));
app.listen(8080, "127.0.0.1");
