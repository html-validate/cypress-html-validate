# cypress-html-validate changelog

## 7.1.0 (2025-01-20)

### Features

- **deps:** support cypress v14 ([e8a35f3](https://gitlab.com/html-validate/cypress-html-validate/commit/e8a35f37b460c48aaa5e56d247dbbd4e52f97aef))

### Bug Fixes

- **deps:** update dependency html-validate to v9 ([40d028c](https://gitlab.com/html-validate/cypress-html-validate/commit/40d028c7dcd6d41e4ce8b012e49251b5c7d15aff))

## 7.0.0 (2024-12-15)

### ⚠ BREAKING CHANGES

- **deps:** require nodejs v18 or later
- **deps:** require cypress v11 or later
- **deps:** require html-validate v8 or later

### Features

- **deps:** require cypress v11 or later ([19681ed](https://gitlab.com/html-validate/cypress-html-validate/commit/19681ed10b6f9ceac0f409c9d31f6dffe6584b1e))
- **deps:** require html-validate v8 or later ([d17d7d6](https://gitlab.com/html-validate/cypress-html-validate/commit/d17d7d62d29fee305f40f5af0d081495c2c853c5))
- **deps:** require nodejs v18 or later ([c33bae1](https://gitlab.com/html-validate/cypress-html-validate/commit/c33bae1ac1dd1957aa7365eec487e3fa49c8e87c))
- **deps:** support html-validate v9 ([1b8a014](https://gitlab.com/html-validate/cypress-html-validate/commit/1b8a014851245c78f2c64d79a8230db0f81e3e9a))

## [6.1.0](https://gitlab.com/html-validate/cypress-html-validate/compare/v6.0.2...v6.1.0) (2023-09-09)

### Features

- **deps:** support cypress v13 ([153eb2e](https://gitlab.com/html-validate/cypress-html-validate/commit/153eb2ec0e253ec5c67e39a9795193f0332b9584))

## [6.0.2](https://gitlab.com/html-validate/cypress-html-validate/compare/v6.0.1...v6.0.2) (2023-07-16)

### Bug Fixes

- fix webpack `??` null coalescing error ([a05db3b](https://gitlab.com/html-validate/cypress-html-validate/commit/a05db3b613bbd699d8f218e6738abafe91ec3765))

## [6.0.1](https://gitlab.com/html-validate/cypress-html-validate/compare/v6.0.0...v6.0.1) (2023-07-15)

### Bug Fixes

- drop support for deprecated `void` rule ([ade435c](https://gitlab.com/html-validate/cypress-html-validate/commit/ade435cc1b518c0aee3a320d4d54939624d0e373))

## [6.0.0](https://gitlab.com/html-validate/cypress-html-validate/compare/v5.1.2...v6.0.0) (2023-06-09)

### ⚠ BREAKING CHANGES

- **deps:** require html-validate v5 or later
- **deps:** require cypress v10 or later
- **deps:** require nodejs v16 or later

### Features

- **deps:** require cypress v10 or later ([7e3acec](https://gitlab.com/html-validate/cypress-html-validate/commit/7e3acecae9ed068ff9dc43a239598b6ec34eb6c8))
- **deps:** require html-validate v5 or later ([f9524e6](https://gitlab.com/html-validate/cypress-html-validate/commit/f9524e69deea01000d8b62242ffc1ceb1eefcbce))
- **deps:** require nodejs v16 or later ([36448a8](https://gitlab.com/html-validate/cypress-html-validate/commit/36448a8866c9e856c5091466cb5cc3e5fde81580))

### Dependency upgrades

- **deps:** support html-validate v8 ([8a0309d](https://gitlab.com/html-validate/cypress-html-validate/commit/8a0309dcbe159d6a4e843d209d23cab3f5074d29))

## [5.1.2](https://gitlab.com/html-validate/cypress-html-validate/compare/v5.1.1...v5.1.2) (2023-01-15)

### Dependency upgrades

- **deps:** support `no-unused-disable` from html-validate to v7.13.0 ([b2188d9](https://gitlab.com/html-validate/cypress-html-validate/commit/b2188d975bb414e00b8a50ade9bf7e25303f692b))

## [5.1.1](https://gitlab.com/html-validate/cypress-html-validate/compare/v5.1.0...v5.1.1) (2022-12-09)

### Dependency upgrades

- **deps:** update dependency cypress to v12 ([34f1d16](https://gitlab.com/html-validate/cypress-html-validate/commit/34f1d164087c9c41ca2ba0a8ad7a0cdc529f66b8))

## [5.1.0](https://gitlab.com/html-validate/cypress-html-validate/compare/v5.0.5...v5.1.0) (2022-11-20)

### Features

- **deps:** support cypress v11 ([6c19c57](https://gitlab.com/html-validate/cypress-html-validate/commit/6c19c57947a0e5fa8f778db013d685f1bd0ff334))

## [5.0.5](https://gitlab.com/html-validate/cypress-html-validate/compare/v5.0.4...v5.0.5) (2022-09-08)

### Bug Fixes

- fix `on` function signature ([e33482c](https://gitlab.com/html-validate/cypress-html-validate/commit/e33482c6ec4ab18e0a2436ff3352ee1ddf71fae0)), closes [#16](https://gitlab.com/html-validate/cypress-html-validate/issues/16)

## [5.0.4](https://gitlab.com/html-validate/cypress-html-validate/compare/v5.0.3...v5.0.4) (2022-08-05)

### Bug Fixes

- import without `dist` in path works again ([da569a4](https://gitlab.com/html-validate/cypress-html-validate/commit/da569a421bc7921e1af644c06b876944a5f8f5e5)), closes [#15](https://gitlab.com/html-validate/cypress-html-validate/issues/15)

## [5.0.3](https://gitlab.com/html-validate/cypress-html-validate/compare/v5.0.2...v5.0.3) (2022-07-16)

### Bug Fixes

- fix broken typescript declaration for `commands` ([7feec4d](https://gitlab.com/html-validate/cypress-html-validate/commit/7feec4dad8a5899f049ec12baeab91cd9cb949e1)), closes [#14](https://gitlab.com/html-validate/cypress-html-validate/issues/14)

## [5.0.2](https://gitlab.com/html-validate/cypress-html-validate/compare/v5.0.1...v5.0.2) (2022-06-21)

### Bug Fixes

- export formatter function ([e921a73](https://gitlab.com/html-validate/cypress-html-validate/commit/e921a739a11732bd9735c23d5ba930a74658bfb4))

## [5.0.1](https://gitlab.com/html-validate/cypress-html-validate/compare/v5.0.0...v5.0.1) (2022-06-21)

### Bug Fixes

- add old paths to exports definition ([6f7d7ac](https://gitlab.com/html-validate/cypress-html-validate/commit/6f7d7ac58d3ee9618e3e80114f4878a3d96db869))

## [5.0.0](https://gitlab.com/html-validate/cypress-html-validate/compare/v4.0.0...v5.0.0) (2022-06-20)

### ⚠ BREAKING CHANGES

- require Cypress v7 or later

### Features

- cypress 10 compatibility ([d126e2c](https://gitlab.com/html-validate/cypress-html-validate/commit/d126e2cb2b8a996d42ccd04c1ba68ce2f5ffa2af)), closes [html-validate#156](https://gitlab.com/html-validate/html-validate/issues/156)
- require Cypress v7 or later ([2cd4809](https://gitlab.com/html-validate/cypress-html-validate/commit/2cd4809894cf51fe2f5ea38ae09512377e6678c3))
- support node exports field ([55b4f10](https://gitlab.com/html-validate/cypress-html-validate/commit/55b4f10543a4d7144d4ed984f09d8c5f3157f6b3))

## [4.0.0](https://gitlab.com/html-validate/cypress-html-validate/compare/v3.0.0...v4.0.0) (2022-05-16)

### ⚠ BREAKING CHANGES

- **deps:** drop support for cypress v3

### Features

- allow using `cy.get(..).htmlvalidate()` to validate only selected subject(s) ([a4db2c8](https://gitlab.com/html-validate/cypress-html-validate/commit/a4db2c86d55e7bdaf5b257c042d44781f1213d9f)), closes [html-validate#150](https://gitlab.com/html-validate/html-validate/issues/150)
- **deps:** drop support for cypress v3 ([3ef16aa](https://gitlab.com/html-validate/cypress-html-validate/commit/3ef16aaf596c2574f2986c531ed05818be29b447))

## [3.0.0](https://gitlab.com/html-validate/cypress-html-validate/compare/v2.1.3...v3.0.0) (2022-05-08)

### ⚠ BREAKING CHANGES

- require node 14

### Features

- require node 14 ([152c934](https://gitlab.com/html-validate/cypress-html-validate/commit/152c934909121355facc78fee39524abb07941df))

### Dependency upgrades

- **deps:** support html-validate v7 ([f0f12af](https://gitlab.com/html-validate/cypress-html-validate/commit/f0f12afad430c3677682ea85b3cf0baadbf4fe04))

### [2.1.3](https://gitlab.com/html-validate/cypress-html-validate/compare/v2.1.2...v2.1.3) (2022-02-06)

### Bug Fixes

- **typescript:** export options type via main field ([883b03e](https://gitlab.com/html-validate/cypress-html-validate/commit/883b03e1de1d5948c1d1b09cb36585611595a086))

### [2.1.2](https://gitlab.com/html-validate/cypress-html-validate/compare/v2.1.1...v2.1.2) (2021-11-14)

### Dependency upgrades

- **deps:** update dependency cypress to v9 ([b96fd3f](https://gitlab.com/html-validate/cypress-html-validate/commit/b96fd3fe3cc9e89f95514d5fa373ca886ec356b0))

### [2.1.1](https://gitlab.com/html-validate/cypress-html-validate/compare/v2.1.0...v2.1.1) (2021-09-27)

### Dependency upgrades

- **deps:** update dependency html-validate to v6 ([4e32463](https://gitlab.com/html-validate/cypress-html-validate/commit/4e32463660ff351f8a7d76c560e16cc5aecf38b1))

## [2.1.0](https://gitlab.com/html-validate/cypress-html-validate/compare/v2.0.2...v2.1.0) (2021-09-17)

### Features

- support passing config and options to each validator call ([30e78ff](https://gitlab.com/html-validate/cypress-html-validate/commit/30e78ff4ef53ff9a98da1c97b6d1e8d0032ffeeb)), closes [#9](https://gitlab.com/html-validate/cypress-html-validate/issues/9)

### [2.0.2](https://gitlab.com/html-validate/cypress-html-validate/compare/v2.0.1...v2.0.2) (2021-07-23)

### Dependency upgrades

- **deps:** update dependency cypress to v8 ([9630137](https://gitlab.com/html-validate/cypress-html-validate/commit/963013783a7087f039a5e135d28640d3e1769c40))

### [2.0.1](https://gitlab.com/html-validate/cypress-html-validate/compare/v2.0.0...v2.0.1) (2021-06-27)

### Dependency upgrades

- **deps:** update dependency html-validate to v5 ([9d7076c](https://gitlab.com/html-validate/cypress-html-validate/commit/9d7076c07df212291e549e1e29b73710ada13de2))

## [2.0.0](https://gitlab.com/html-validate/cypress-html-validate/compare/v1.5.1...v2.0.0) (2021-06-27)

### ⚠ BREAKING CHANGES

- require NodeJS 12

### Features

- require NodeJS 12 ([705c004](https://gitlab.com/html-validate/cypress-html-validate/commit/705c00461f90c85be11a1cfc61d34fd3422fe26f))

### [1.5.1](https://gitlab.com/html-validate/cypress-html-validate/compare/v1.5.0...v1.5.1) (2021-05-14)

### Bug Fixes

- configuration merging not overwriting extends ([4fe9cd7](https://gitlab.com/html-validate/cypress-html-validate/commit/4fe9cd7cd845d87a3d80ac92b55c42d13514f65a)), closes [#7](https://gitlab.com/html-validate/cypress-html-validate/issues/7)

## [1.5.0](https://gitlab.com/html-validate/cypress-html-validate/compare/v1.4.1...v1.5.0) (2021-04-07)

### Features

- cypress 7 compatibility ([6959930](https://gitlab.com/html-validate/cypress-html-validate/commit/6959930f6c6ef511eda0daf07d90d56059aa589b)), closes [#6](https://gitlab.com/html-validate/cypress-html-validate/issues/6)

### [1.4.1](https://gitlab.com/html-validate/cypress-html-validate/compare/v1.4.0...v1.4.1) (2021-03-16)

### Bug Fixes

- "Converting circular structure to JSON" when `HTMLElement` was serialized ([1a216a3](https://gitlab.com/html-validate/cypress-html-validate/commit/1a216a323700c710e47ace18e8d6987394cdd63f)), closes [#5](https://gitlab.com/html-validate/cypress-html-validate/issues/5)

## [1.4.0](https://gitlab.com/html-validate/cypress-html-validate/compare/v1.3.1...v1.4.0) (2021-02-21)

### Features

- add console output support ([d484863](https://gitlab.com/html-validate/cypress-html-validate/commit/d484863e0955baefef737c7f16d9a9877fe91aed)), closes [#4](https://gitlab.com/html-validate/cypress-html-validate/issues/4)

### Bug Fixes

- error message highlights elements in browser again ([a643111](https://gitlab.com/html-validate/cypress-html-validate/commit/a643111fa26497ad750b4d99d2e58d3bdc8a5090))

### [1.3.1](https://gitlab.com/html-validate/cypress-html-validate/compare/v1.3.0...v1.3.1) (2020-12-06)

### Dependency upgrades

- **deps:** update dependency cypress to v6 ([6cc1256](https://gitlab.com/html-validate/cypress-html-validate/commit/6cc125689eae4669fca4622b958b9a6e168a200b))

## [1.3.0](https://gitlab.com/html-validate/cypress-html-validate/compare/v1.2.5...v1.3.0) (2020-11-08)

### Features

- disable `void-style` by default ([51314e3](https://gitlab.com/html-validate/cypress-html-validate/commit/51314e3b80e3efb8c3fa0f89db00e036f6d55d14))
- html-validate v4 compatibility ([7b66a34](https://gitlab.com/html-validate/cypress-html-validate/commit/7b66a344df8ff2faa3b15e4c0451c34a08da88cf))

## [1.2.5](https://gitlab.com/html-validate/cypress-html-validate/compare/v1.2.4...v1.2.5) (2020-11-01)

## [1.2.4](https://gitlab.com/html-validate/cypress-html-validate/compare/v1.2.3...v1.2.4) (2020-10-25)

## [1.2.3](https://gitlab.com/html-validate/cypress-html-validate/compare/v1.2.2...v1.2.3) (2020-09-30)

## [1.2.2](https://gitlab.com/html-validate/cypress-html-validate/compare/v1.2.1...v1.2.2) (2020-06-09)

### Bug Fixes

- selectors in `<head>` matches DOM ([674544b](https://gitlab.com/html-validate/cypress-html-validate/commit/674544bd369fd2f69540659aff65f0829df4f50e)), closes [#1](https://gitlab.com/html-validate/cypress-html-validate/issues/1)

## [1.2.1](https://gitlab.com/html-validate/cypress-html-validate/compare/v1.2.0...v1.2.1) (2020-05-29)

### Bug Fixes

- disable `attribute-empty-style` ([a2fb02b](https://gitlab.com/html-validate/cypress-html-validate/commit/a2fb02bc3832622987f3663e6f444b9f105148b8)), closes [#1](https://gitlab.com/html-validate/cypress-html-validate/issues/1)

# [1.2.0](https://gitlab.com/html-validate/cypress-html-validate/compare/v1.1.1...v1.2.0) (2020-02-26)

### Features

- support excluding/including errors via selectors ([32ec9dc](https://gitlab.com/html-validate/cypress-html-validate/commit/32ec9dc87db9c86909edb26961111117f7ff1e1d))

## [1.1.1](https://gitlab.com/html-validate/cypress-html-validate/compare/v1.1.0...v1.1.1) (2020-02-17)

### Bug Fixes

- **config:** use new void rules ([1b585d3](https://gitlab.com/html-validate/cypress-html-validate/commit/1b585d3110533981201c88b570649796e82c712a))

# [1.1.0](https://gitlab.com/html-validate/cypress-html-validate/compare/v1.0.0...v1.1.0) (2020-01-26)

### Features

- lookup dom elements if selector is present ([4a25877](https://gitlab.com/html-validate/cypress-html-validate/commit/4a25877f44038e506092340d438deac8b68c83c0))

# 1.0.0 (2020-01-24)

### Features

- initial version ([c682dec](https://gitlab.com/html-validate/cypress-html-validate/commit/c682decb0c05f4a99dccc6b2ccc01bdc2346977d))
