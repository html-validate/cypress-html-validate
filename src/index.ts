import { type ConfigData } from "html-validate";

export { type CypressHtmlValidateOptions } from "./options";
export { formatter } from "./formatter";
export { type ConfigData };
