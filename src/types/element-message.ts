import type { Message } from "html-validate";

/**
 * Message augmented with HTMLElement it refers to.
 */
export interface ElementMessage extends Message {
	element: HTMLElement | null;
}
