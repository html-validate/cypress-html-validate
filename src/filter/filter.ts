export interface FilterOptions {
	include: string[];
	exclude: string[];
}

function findElements(document: Document, selectors: string[]): Element[][] {
	return selectors.map((selector) => {
		return Array.from(document.querySelectorAll(selector));
	});
}

function matchSelectors(element: Element, selector: Element[][]): boolean {
	return selector.some((elements) => {
		return elements.some((inner) => inner.contains(element));
	});
}

/**
 * Element filtering.
 *
 * Takes a list of selectors to include/exclude and matches element against
 * these lists.
 *
 * @internal
 */
export class Filter {
	private include: Element[][];
	private exclude: Element[][];
	private matchInclude: boolean;
	private matchExclude: boolean;

	public constructor(document: Document, { include, exclude }: FilterOptions) {
		this.include = findElements(document, include);
		this.exclude = findElements(document, exclude);
		this.matchInclude = include.length > 0;
		this.matchExclude = exclude.length > 0;
	}

	/**
	 * Match element against filter.
	 *
	 * Returns `false` if the element matches:
	 *
	 *   - none of include selectors
	 *   - one or more of exclude selectors
	 *
	 * Empty lists are not considered (in particular, if `include` is empty it is
	 * the same as a selector to match the entire document).
	 */
	public match(element: Element): boolean {
		const { exclude, include } = this;
		if (this.matchExclude && matchSelectors(element, exclude)) {
			return false;
		}
		if (this.matchInclude) {
			return matchSelectors(element, include);
		}
		return true;
	}
}
