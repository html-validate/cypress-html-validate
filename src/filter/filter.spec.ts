/**
 * @jest-environment jsdom
 */

import { Filter } from "./filter";

beforeEach(() => {
	document.body.innerHTML = /* HTML */ `
		<header>
			<p>
				<em id="header-text">Lorem ipsum</em>
			</p>
		</header>

		<main>
			<div id="app">
				<p>
					<em id="main-text">Lorem ipsum</em>
				</p>
			</div>
		</main>

		<footer>
			<p>
				<em id="footer-text">Lorem ipsum</em>
			</p>
		</footer>
	`;
});

it("should return true if both lists are empty", () => {
	expect.assertions(1);
	const em = Array.from(document.querySelectorAll("em"));
	const filter = new Filter(document, { include: [], exclude: [] });
	const matched = em.filter((it) => filter.match(it)).map((it) => it.id);
	expect(matched).toEqual(["header-text", "main-text", "footer-text"]);
});

it('should remove elements outside selectors from "include"', () => {
	expect.assertions(1);
	const em = Array.from(document.querySelectorAll("em"));
	const filter = new Filter(document, { include: ["header", "footer"], exclude: [] });
	const matched = em.filter((it) => filter.match(it)).map((it) => it.id);
	expect(matched).toEqual(["header-text", "footer-text"]);
});

it('should remove elements inside selectors from "exclude"', () => {
	expect.assertions(1);
	const em = Array.from(document.querySelectorAll("em"));
	const filter = new Filter(document, { include: [], exclude: ["header", "footer"] });
	const matched = em.filter((it) => filter.match(it)).map((it) => it.id);
	expect(matched).toEqual(["main-text"]);
});

it('should handle when "include" matches element exactly', () => {
	expect.assertions(1);
	const em = Array.from(document.querySelectorAll("em"));
	const filter = new Filter(document, { include: ["header em"], exclude: [] });
	const matched = em.filter((it) => filter.match(it)).map((it) => it.id);
	expect(matched).toEqual(["header-text"]);
});

it('should handle when "exclude" matches element exactly', () => {
	expect.assertions(1);
	const em = Array.from(document.querySelectorAll("em"));
	const filter = new Filter(document, { include: [], exclude: ["header em"] });
	const matched = em.filter((it) => filter.match(it)).map((it) => it.id);
	expect(matched).toEqual(["main-text", "footer-text"]);
});

it('should handle when "include" matches no selectors in the document', () => {
	expect.assertions(1);
	const em = Array.from(document.querySelectorAll("em"));
	const filter = new Filter(document, { include: ["missing"], exclude: [] });
	const matched = em.filter((it) => filter.match(it)).map((it) => it.id);
	expect(matched).toEqual([]);
});

it('should handle when "exclude" matches no selectors in the document', () => {
	expect.assertions(1);
	const em = Array.from(document.querySelectorAll("em"));
	const filter = new Filter(document, { include: [], exclude: ["missing"] });
	const matched = em.filter((it) => filter.match(it)).map((it) => it.id);
	expect(matched).toEqual(["header-text", "main-text", "footer-text"]);
});
