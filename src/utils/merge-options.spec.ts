import { CypressHtmlValidateOptions } from "../options";
import { mergeOptions } from "./merge-options";

it("should merge options", () => {
	expect.assertions(1);
	const global: CypressHtmlValidateOptions = {
		include: ["foo"],
		exclude: [],
		formatter() {
			/* do nothing */
		},
	};
	const local: Partial<CypressHtmlValidateOptions> = {
		exclude: ["bar"],
	};
	expect(mergeOptions({ global, local, subject: undefined })).toEqual({
		include: ["foo"],
		exclude: ["bar"],
		formatter: expect.anything(),
	});
});

it("should overwrite arrays", () => {
	expect.assertions(1);
	const global: CypressHtmlValidateOptions = {
		include: ["foo"],
		exclude: [],
		formatter() {
			/* do nothing */
		},
	};
	const local: Partial<CypressHtmlValidateOptions> = {
		include: ["bar"],
	};
	expect(mergeOptions({ global, local, subject: undefined })).toEqual({
		include: ["bar"],
		exclude: [],
		formatter: expect.anything(),
	});
});

describe("with subject", () => {
	it("should overwrite global include", () => {
		expect.assertions(1);
		const global: CypressHtmlValidateOptions = {
			include: ["foo"],
			exclude: [],
			formatter() {
				/* do nothing */
			},
		};
		const local: Partial<CypressHtmlValidateOptions> = {};
		const subject = "baz";
		expect(mergeOptions({ global, local, subject })).toEqual({
			include: ["baz"],
			exclude: [],
			formatter: expect.anything(),
		});
	});

	it("should prefix selectors", () => {
		expect.assertions(1);
		const global: CypressHtmlValidateOptions = {
			include: ["foo"],
			exclude: ["bar"],
			formatter() {
				/* do nothing */
			},
		};
		const local: Partial<CypressHtmlValidateOptions> = {
			include: [".include .me"],
			exclude: [".exclude .me"],
		};
		const subject = "my-subject";
		expect(mergeOptions({ global, local, subject })).toEqual({
			include: ["my-subject .include .me"],
			exclude: ["my-subject .exclude .me"],
			formatter: expect.anything(),
		});
	});
});
