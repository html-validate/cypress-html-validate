/**
 * @jest-environment jsdom
 */

import { getPageSource } from "./get-page-source";
import * as fixtures from "./__fixtures__";

// https://github.com/cypress-io/cypress/blob/master/packages/proxy/lib/http/util/inject.ts
const INJECTED_SCRIPT =
	"<script type='text/javascript'> document.domain = 'example.net'; var Cypress = window.Cypress = parent.Cypress; if (!Cypress) { throw new Error('...'); }; Cypress.action('app:window:before:load', window); </script>";

it.each(Object.entries(fixtures))(
	"should ignore cypress-injected (%s) script tag",
	(_: string, script: string) => {
		expect.assertions(1);
		document.documentElement.innerHTML = `
		<head>
			${script}
		</head>
		<body></body>
	`;
		expect(getPageSource(document)).toMatchSnapshot();
	},
);

it("should not touch other scripts in head", () => {
	expect.assertions(1);
	document.documentElement.innerHTML = `
		<head>
			${INJECTED_SCRIPT}
			<script type="text/javascript">foo</script>
			<script>bar</script>
		</head>
		<body></body>
	`;
	expect(getPageSource(document)).toMatchInlineSnapshot(`
		"<html><head>
					<!-- [html-validate-disable-next no-unused-disable, require-sri, script-type] -->
					<script type="text/javascript"> document.domain = 'example.net'; var Cypress = window.Cypress = parent.Cypress; if (!Cypress) { throw new Error('...'); }; Cypress.action('app:window:before:load', window); </script>
					<script type="text/javascript">foo</script>
					<script>bar</script>
				</head>
				<body>
			</body></html>"
	`);
});
