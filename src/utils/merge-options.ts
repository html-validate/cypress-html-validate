import { type CypressHtmlValidateOptions } from "../options";

export interface MergeSources {
	global: CypressHtmlValidateOptions;
	local: Partial<CypressHtmlValidateOptions>;
	subject: string | null | undefined;
}

export function joinSelector(a: string, b: string): string {
	return [a, b].join(" ");
}

export function mergeOptions({
	global,
	local = {},
	subject,
}: MergeSources): CypressHtmlValidateOptions {
	if (subject) {
		const merged = {
			...global,
			...{
				include: [subject],
			},
		};
		if (local.include) {
			merged.include = local.include.map((it) => joinSelector(subject, it));
		}
		if (local.exclude) {
			merged.exclude = local.exclude.map((it) => joinSelector(subject, it));
		}
		return merged;
	} else {
		return {
			...global,
			...local,
		};
	}
}
