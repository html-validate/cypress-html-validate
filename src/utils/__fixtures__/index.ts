import fs from "fs";
import path from "path";

function readFixture(filename: string): string {
	const resolved = path.join(__dirname, filename);
	return fs.readFileSync(resolved, "utf-8");
}

export const injectedV3 = readFixture("injected-v3.html");
export const injectedV4 = readFixture("injected-v4.html");
export const injectedV5 = readFixture("injected-v5.html");
export const injectedV6 = readFixture("injected-v6.html");
export const injectedV7 = readFixture("injected-v7.html");
