export function getPageSource(document: Document): string {
	const matchCypress = new RegExp(
		/* eslint-disable-next-line sonarjs/slow-regex -- technical debt, could be refactored */
		"(\\s*)<script type=[\"']text/javascript[\"']>.*?window.Cypress\\s*=\\s*parent.Cypress.*?</script>",
	);
	const root = document.documentElement;
	return root.outerHTML.replace(matchCypress, (match: string, indent: string) => {
		/* force disable rules for the cypress injected script tag */
		return [
			`${indent}<!-- [html-validate-disable-next no-unused-disable, require-sri, script-type] -->`,
			match,
		].join("");
	});
}
