/**
 * Returns the first parameters that matches predicate or `undefined` if no
 * match is found.
 */
export function getArg<S, T extends S>(is: (val: S) => val is T, ...args: S[]): T | undefined {
	return args.find<T>((it): it is T => is(it));
}
