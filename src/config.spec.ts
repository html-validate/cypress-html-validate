import defaultConfig, { createConfig, mergeConfig } from "./config";

it("snapshot", () => {
	expect.assertions(1);
	expect(createConfig()).toMatchInlineSnapshot(`
		{
		  "extends": [
		    "html-validate:recommended",
		    "html-validate:document",
		  ],
		  "root": true,
		  "rules": {
		    "attribute-boolean-style": "off",
		    "attribute-empty-style": "off",
		    "missing-doctype": "off",
		    "no-inline-style": "off",
		    "no-self-closing": "off",
		    "no-trailing-whitespace": "off",
		    "void-style": "off",
		  },
		}
	`);
});

describe("mergeConfig", () => {
	it("should handle missing param", () => {
		expect.assertions(1);
		expect(mergeConfig(defaultConfig).extends).toEqual([
			"html-validate:recommended",
			"html-validate:document",
		]);
	});

	it("extends should be overwritten", () => {
		expect.assertions(3);
		expect(mergeConfig(defaultConfig, {}).extends).toEqual([
			"html-validate:recommended",
			"html-validate:document",
		]);
		expect(mergeConfig(defaultConfig, { extends: ["html-validate:standard"] }).extends).toEqual([
			"html-validate:standard",
		]);
		expect(mergeConfig(defaultConfig, { extends: [] }).extends).toEqual([]);
	});
});
