import { type Message } from "html-validate";
import { formatter } from "./formatter";

export interface CypressHtmlValidateOptions {
	exclude: string[];
	include: string[];
	formatter?: (messages: Message[]) => void;
}

const options: CypressHtmlValidateOptions = {
	exclude: [],
	include: [],
	formatter,
};

export default options;
