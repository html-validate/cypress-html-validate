import { Console } from "console";
import { WritableStreamBuffer } from "stream-buffers";
import { type Message } from "html-validate";
import { formatter } from "./formatter";

let consoleBuffer: WritableStreamBuffer;
let console: Console;
let originalConsole: Console;

beforeEach(() => {
	consoleBuffer = new WritableStreamBuffer();
	console = new Console({
		stdout: consoleBuffer,
		stderr: consoleBuffer,
		colorMode: false,
	});
	originalConsole = global.console;
	global.console = console;
});

afterEach(() => {
	global.console = originalConsole;
});

it("should format when 1 error is present", () => {
	expect.assertions(1);
	const errors: Message[] = [
		{
			ruleId: "mock-rule",
			severity: 2,
			message: "Mock message",
			selector: "body > div",
			line: 1,
			column: 2,
			offset: 3,
			size: 1,
		},
	];
	formatter(errors);
	expect(consoleBuffer.getContentsAsString("utf-8")).toMatchInlineSnapshot(`
		"1 html-validate error was detected
		┌─────────┬─────────────┬────────────────┬──────────────┐
		│ (index) │    rule     │    message     │   selector   │
		├─────────┼─────────────┼────────────────┼──────────────┤
		│    0    │ 'mock-rule' │ 'Mock message' │ 'body > div' │
		└─────────┴─────────────┴────────────────┴──────────────┘
		"
	`);
});

it("should format when multiple errors are present", () => {
	expect.assertions(1);
	const errors: Message[] = [
		{
			ruleId: "mock-rule",
			severity: 2,
			message: "Mock message",
			selector: "body > div",
			line: 1,
			column: 2,
			offset: 3,
			size: 1,
		},
		{
			ruleId: "another-rule",
			severity: 1,
			message: "Another message",
			selector: "body > p",
			line: 1,
			column: 2,
			offset: 3,
			size: 1,
		},
	];
	formatter(errors);
	formatter(errors);
	expect(consoleBuffer.getContentsAsString("utf-8")).toMatchInlineSnapshot(`
		"2 html-validate errors were detected
		┌─────────┬────────────────┬───────────────────┬──────────────┐
		│ (index) │      rule      │      message      │   selector   │
		├─────────┼────────────────┼───────────────────┼──────────────┤
		│    0    │  'mock-rule'   │  'Mock message'   │ 'body > div' │
		│    1    │ 'another-rule' │ 'Another message' │  'body > p'  │
		└─────────┴────────────────┴───────────────────┴──────────────┘
		2 html-validate errors were detected
		┌─────────┬────────────────┬───────────────────┬──────────────┐
		│ (index) │      rule      │      message      │   selector   │
		├─────────┼────────────────┼───────────────────┼──────────────┤
		│    0    │  'mock-rule'   │  'Mock message'   │ 'body > div' │
		│    1    │ 'another-rule' │ 'Another message' │  'body > p'  │
		└─────────┴────────────────┴───────────────────┴──────────────┘
		"
	`);
});

it("should not output anything when there are no messages", () => {
	expect.assertions(1);
	const errors: Message[] = [];
	formatter(errors);
	expect(consoleBuffer.getContentsAsString("utf-8")).toBeFalsy();
});
