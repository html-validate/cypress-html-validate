/// <reference types="cypress" />

import type { ConfigData, Message, Report } from "html-validate";
import type { ElementMessage } from "./types";
import type { CypressHtmlValidateOptions } from "./options";
import { getPageSource, mergeOptions } from "./utils";
import { getArg } from "./get-arg";
import { Filter } from "./filter";
import { taskFormat, taskGlobalOptions, taskHtmlvalidate } from "./tasks";

declare global {
	/* eslint-disable-next-line @typescript-eslint/no-namespace -- module augmentation */
	export namespace Cypress {
		interface Chainable {
			htmlvalidate(
				localConfig?: ConfigData,
				localOption?: Partial<CypressHtmlValidateOptions>,
			): void;
			htmlvalidate(localOption: Partial<CypressHtmlValidateOptions>): void;
		}
	}
}

function logError(error: Message, $el?: JQuery): void {
	Cypress.log({
		$el,
		name: error.ruleId,
		consoleProps: () => error,
		message: [error.message],
	});
}

function findElements(messages: Message[], document: Document): ElementMessage[] {
	return messages.map((message) => {
		return {
			...message,
			element: message.selector ? document.querySelector(message.selector) : null,
		};
	});
}

function filterMessages(
	messages: ElementMessage[],
	document: Document,
	options: CypressHtmlValidateOptions,
): ElementMessage[] {
	const filter = new Filter(document, options);
	return messages.filter((message) => !message.element || filter.match(message.element));
}

function omitElement(message: ElementMessage): Message {
	const { element, ...rest } = message;
	return rest;
}

function isConfigData(src?: ConfigData | Partial<CypressHtmlValidateOptions>): src is ConfigData {
	if (!src) {
		return false;
	}
	return (
		"root" in src ||
		"extends" in src ||
		"elements" in src ||
		"plugins" in src ||
		"transform" in src ||
		"rules" in src
	);
}

function isOptions(
	src?: ConfigData | Partial<CypressHtmlValidateOptions>,
): src is Partial<CypressHtmlValidateOptions> {
	if (!src) {
		return false;
	}
	return "exclude" in src || "include" in src || "formatter" in src;
}

function isJqueryWithSelector(subject: unknown): subject is Cypress.JQueryWithSelector {
	return Boolean(subject && (subject as { selector?: string }).selector);
}

/**
 * Run html-validate.
 */
function run(markup: string, config?: ConfigData): Cypress.Chainable<Report> {
	return taskHtmlvalidate(markup, config);
}

function getResults(
	document: Document,
	config: ConfigData,
	options: CypressHtmlValidateOptions,
): Cypress.Chainable<[messages: number, filtered: number]> {
	const source = getPageSource(document);
	return run(source, config).then((report: Report) => {
		if (report.valid) {
			return cy.wrap([0, 0], { log: false });
		} else {
			const messages = findElements(report.results[0].messages, document);
			const filtered = filterMessages(messages, document, options);
			cy.wrap(filtered, { log: false }).each((error: ElementMessage) => {
				const $el = error.element ? Cypress.$(error.element) : undefined;
				logError(error, $el);
			});

			/* log violations to nodejs process */
			if (filtered.length > 0) {
				/* some properties in `HTMLElement` might not serialize
				 * properly so they are omitted from the messages before
				 * passing it to the nodejs process */
				const stripped = filtered.map(omitElement);
				taskFormat(stripped);
			}

			return cy.wrap([messages.length, filtered.length], {
				log: false,
			});
		}
	});
}

function validateResult([messages, filtered]: [messages: number, filtered: number]): void {
	const s = filtered !== 1 ? "s" : "";
	const excluded = messages - filtered;
	assert.equal(filtered, 0, `${String(filtered)} error${s}, ${String(excluded)} excluded`);
}

function htmlvalidate(
	subject: unknown,
	localConfig?: ConfigData,
	localOption?: Partial<CypressHtmlValidateOptions>,
): void;
function htmlvalidate(subject: unknown, localOption: Partial<CypressHtmlValidateOptions>): void;
function htmlvalidate(
	subject: unknown,
	arg1?: Partial<CypressHtmlValidateOptions> | ConfigData,
	arg2?: Partial<CypressHtmlValidateOptions>,
): void {
	const localConfig = getArg(isConfigData, arg1) ?? {};
	const localOptions = getArg(isOptions, arg2, arg1) ?? {};

	Cypress.log({
		name: "html-validate",
		message: ["Validating document"],
	});

	cy.document({ log: false }).then((document) => {
		taskGlobalOptions()
			.then((globalOptions) => {
				/* if a subject is passed first make sure the subject is not filtered by
				 * the global configuration */
				if (isJqueryWithSelector(subject)) {
					const globalFilter = new Filter(document, globalOptions);
					const hidden = Array.from(subject).filter((it) => !globalFilter.match(it));
					if (hidden.length > 0) {
						assert.fail(`Selected element is being excluded by global configuration`);
					}
				}

				const mergedOptions = mergeOptions({
					global: globalOptions,
					local: localOptions,
					subject: isJqueryWithSelector(subject) ? subject.selector : undefined,
				});

				Cypress.log({
					name: "options",
					message: [mergedOptions.include, mergedOptions.exclude],
				});

				return getResults(document, localConfig, mergedOptions);
			})
			.then((results) => {
				validateResult(results);
			});
	});
}

Cypress.Commands.add<"htmlvalidate", "optional">(
	"htmlvalidate",
	{
		prevSubject: "optional",
	},
	htmlvalidate,
);
