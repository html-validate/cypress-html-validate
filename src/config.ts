import { type ConfigData } from "html-validate";
import deepmerge from "deepmerge";

function overwriteMerge<T>(_a: T[], b: T[]): T[] {
	return b;
}

export function createConfig(): ConfigData {
	return {
		root: true,

		extends: ["html-validate:recommended", "html-validate:document"],

		rules: {
			/* doctype is not passed when requesting source */
			"missing-doctype": "off",

			/* some frameworks (such as jQuery) often uses inline style, e.g. for
			 * showing/hiding elements */
			"no-inline-style": "off",

			/* scripts will often add markup with trailing whitespace */
			"no-trailing-whitespace": "off",

			/* browser normalizes boolean attributes */
			"attribute-boolean-style": "off",
			"attribute-empty-style": "off",

			/* the browser will often do what it wants, out of users control */
			"void-style": "off",
			"no-self-closing": "off",
		},
	};
}

export function mergeConfig(a: ConfigData, b?: ConfigData): ConfigData {
	return deepmerge(a, b ?? {}, { arrayMerge: overwriteMerge });
}

export default createConfig();
