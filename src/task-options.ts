import type { ConfigData } from "html-validate";

export interface HtmlValidateTaskOptions {
	markup: string;
	config?: ConfigData;
}
