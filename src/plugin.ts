import { HtmlValidate, compatibilityCheck } from "html-validate";
import { type ConfigData, type Message, type Report, type Source } from "html-validate";
import kleur from "kleur";
import defaultConfig, { mergeConfig } from "./config";
import defaultOptions from "./options";
import { type CypressHtmlValidateOptions } from "./options";
import { type HtmlValidateTaskOptions } from "./task-options";

interface PackageJson {
	name: string;
	peerDependencies: Record<string, string>;
}

/* eslint-disable-next-line @typescript-eslint/no-require-imports -- want to import at runtime */
const pkg = require("../package.json") as PackageJson;

export function install(
	/* eslint-disable-next-line @typescript-eslint/no-explicit-any --- should accept any function prototype */
	on: (action: "task", arg: Record<string, (value: any) => any>) => void,
	userConfig?: ConfigData,
	userOptions?: Partial<CypressHtmlValidateOptions>,
): void {
	const range = pkg.peerDependencies["html-validate"];
	compatibilityCheck(pkg.name, range, {
		logger(text: string): void {
			/* default logger uses console.error but cypress swallows those in
			 * headless mode */
			/* eslint-disable-next-line no-console -- expected to log */
			console.log(kleur.red(text));
		},
	});

	const config = mergeConfig(defaultConfig, userConfig);
	const htmlvalidate = new HtmlValidate(config);

	const options = { ...defaultOptions, ...userOptions };

	if (!Array.isArray(options.exclude)) {
		throw new Error(
			`Invalid cypress-html-validate configuration, "exclude" must be array, got ${typeof options.exclude}`,
		);
	}

	if (!Array.isArray(options.include)) {
		throw new Error(
			`Invalid cypress-html-validate configuration, "include" must be array, got ${typeof options.include}`,
		);
	}

	on("task", {
		async htmlvalidate(options: HtmlValidateTaskOptions): Promise<Report> {
			const filename = "cypress";
			const source: Source = {
				data: options.markup,
				filename,
				line: 1,
				column: 1,
				offset: 0,
			};
			/* `validateSource` is used instead of `validateString` for better
			 * backwards compatibility. When targeting only 3.4 or later this can be
			 * switched back to `validateString` */
			return await htmlvalidate.validateSource(source, options.config);
		},
		"htmlvalidate:options"(): CypressHtmlValidateOptions {
			return options;
		},
		"htmlvalidate:format"(messages: Message[]): null {
			const { formatter } = options;
			if (formatter) {
				formatter(messages);
			}
			return null;
		},
	});
}

export default {
	install,
};
