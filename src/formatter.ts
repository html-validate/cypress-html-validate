/* eslint-disable no-console -- expected to log */

import type { Message } from "html-validate";

export function formatter(messages: Message[]): void {
	if (messages.length === 0) {
		return;
	}

	const rows = messages.map((it) => {
		return {
			rule: it.ruleId,
			message: it.message,
			selector: it.selector,
		};
	});
	const n = messages.length;
	const preamble = `${String(n)} html-validate error${n === 1 ? "" : "s"} ${
		n === 1 ? "was" : "were"
	} detected`;

	console.log(preamble);
	console.table(rows);
}
