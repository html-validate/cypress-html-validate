import { type ConfigData, type Report } from "html-validate";
import { type HtmlValidateTaskOptions } from "../task-options";

/**
 * Run html-validate.
 */
export function taskHtmlvalidate(markup: string, config?: ConfigData): Cypress.Chainable<Report> {
	const options: HtmlValidateTaskOptions = {
		markup,
		config,
	};
	return cy.task<Report>("htmlvalidate", options, { log: false });
}
