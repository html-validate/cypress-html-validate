import { type Message } from "html-validate";

/**
 * Format and output messages, e.g. on NodeJS terminal.
 */
export function taskFormat(messages: Message[]): Cypress.Chainable<null> {
	return cy.task<null>("htmlvalidate:format", messages, { log: false });
}
