import { type CypressHtmlValidateOptions } from "../options";

/**
 * Get global options.
 */
export function taskGlobalOptions(): Cypress.Chainable<CypressHtmlValidateOptions> {
	return cy.task<CypressHtmlValidateOptions>("htmlvalidate:options", null, { log: false });
}
