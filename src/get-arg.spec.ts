import { getArg } from "./get-arg";

it("should return the first argument matching predicate", () => {
	expect.assertions(1);
	function predicate(val: string): val is "bar" {
		return val === "bar";
	}
	const values: string[] = ["foo", "bar", "baz"];
	expect(getArg(predicate, ...values)).toBe("bar");
});

it("should return undefined if no argument matches predicate", () => {
	expect.assertions(1);
	function predicate(val: string): val is "spam" {
		return val === "spam";
	}
	const values: string[] = ["foo", "bar", "baz"];
	expect(getArg(predicate, ...values)).toBeUndefined();
});
