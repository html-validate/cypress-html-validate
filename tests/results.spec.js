/* load cypress results and create a flat object with full spec name -> spec
 * (file is created by cypress) */
const results = require("./results.json");

it.each(Object.entries(results))("%s", (_, result) => {
	expect.assertions(1);
	expect(result).toMatchSnapshot();
});
