const fs = require("fs");

/**
 * @typedef {object} Options
 * @property {string} filename
 */

/**
 * @typedef {object} Result
 * @property {"passed" | "failed"} result
 * @property {string} [error]
 */

const defaults = {
	filename: "results.json",
};

/**
 * @returns {string | undefined}
 */
function getError(test) {
	if (!test.displayError) {
		return undefined;
	}
	const lines = test.displayError.split("\n");
	const first = lines[0];
	return first.replace(/^AssertionError: /, "").trim();
}

/**
 * @returns {Result}
 */
function getResult(test) {
	return {
		result: test.state,
		error: getError(test),
	};
}

/**
 * @param {Options} options
 */
function install(on, options) {
	options = { ...defaults, ...options };

	/** @type {Map<string, Result>} */
	let aggregated;

	on("before:run", () => {
		aggregated = new Map();
	});

	on("after:spec", (spec, results) => {
		for (const test of results.tests) {
			const title = test.title.join(" ");
			aggregated.set(title, getResult(test));
		}
	});

	on("after:run", () => {
		const content = JSON.stringify(Object.fromEntries(aggregated), null, 2);
		fs.writeFileSync(options.filename, `${content}\n`);

		/* eslint-disable-next-line no-console -- expected to log */
		console.log("results written to", options.filename);
	});
}

module.exports = {
	install,
};
