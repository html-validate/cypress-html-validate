import * as path from "path";
import { defineConfig } from "cypress";
import htmlvalidate from "../../dist/plugin";
import * as reporter from "../reporter";

export default defineConfig({
	fixturesFolder: path.join(__dirname, "cypress/fixtures"),
	screenshotsFolder: "temp/screenshots",
	screenshotOnRunFailure: false,
	video: false,
	experimentalInteractiveRunEvents: true,
	e2e: {
		setupNodeEvents(on) {
			htmlvalidate.install(on, null, {
				exclude: [".excluded"],
				include: ["#app"],
			});
			reporter.install(on, {
				filename: path.join(__dirname, "../../tests/results.json"),
			});
		},
		baseUrl: "http://localhost:8080",
		specPattern: path.join(__dirname, "cypress/integration/**/*.cy.{js,jsx,ts,tsx}"),
		supportFile: path.join(__dirname, "cypress/support/index.js"),
	},
});
