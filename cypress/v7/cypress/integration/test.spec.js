describe("general", () => {
	it("[PASS] should not report error when a valid page is loaded", () => {
		cy.visit("/valid.html");
		cy.htmlvalidate();
	});

	it("[FAIL] should report error when invalid page is loaded", () => {
		cy.visit("/invalid.html");
		cy.htmlvalidate();
	});

	it("[FAIL] should report error when an action causes document to be invalid", () => {
		cy.visit("/valid.html");
		cy.htmlvalidate();
		cy.get("#insert-invalid").click();
		cy.htmlvalidate();
	});

	it("[PASS] should not report excluded errors", () => {
		cy.visit("/excluded.html");
		cy.htmlvalidate();
	});

	it("[PASS] should not report errors outside included elements", () => {
		cy.visit("/included.html");
		cy.htmlvalidate();
	});

	describe("with subject", () => {
		it("[PASS] should not report error when selecting valid element", () => {
			cy.visit("/invalid.html");
			cy.get("h1").htmlvalidate();
		});

		it("[FAIL] should report error when selecting invalid element", () => {
			cy.visit("/invalid.html");
			cy.get("form").htmlvalidate();
		});

		it("[FAIL] should fail if subject is inside globally excluded element", () => {
			cy.visit("/excluded.html");
			cy.get("p").htmlvalidate();
		});

		it("[FAIL] should fail if subject is outside globally included element", () => {
			cy.visit("/excluded.html");
			cy.get("title").htmlvalidate();
		});

		it("[PASS] should pass if subject is included and not excluded", () => {
			cy.visit("/excluded.html");
			cy.get("h1").htmlvalidate();
		});

		it("[FAIL] should fail if subject is invalid", () => {
			cy.visit("/invalid.html");
			cy.get("form").htmlvalidate();
		});

		it("[FAIL] should fail if descendant of subject is invalid", () => {
			cy.visit("/invalid.html");
			cy.get("body").htmlvalidate();
		});
	});
});
