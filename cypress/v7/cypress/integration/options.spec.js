describe("options", () => {
	it("[PASS] should support passing custom configuration", () => {
		cy.visit("/invalid.html");
		cy.htmlvalidate({
			rules: {
				"wcag/h32": "off",
			},
		});
	});

	it("[PASS] should support passing custom options", () => {
		cy.visit("/invalid.html");
		cy.htmlvalidate({
			exclude: ["form"],
		});
	});

	it("[FAIL] should support passing both config and options", () => {
		cy.visit("/invalid.html");
		cy.htmlvalidate(
			{
				rules: {
					"id-pattern": ["error", { pattern: "foo-.*" }],
				},
			},
			{
				exclude: ["form"],
			},
		);
	});
});
